package com.crazyfriday.lastfm.network

import java.io.IOException

class NoNetworkException: IOException()