package com.crazyfriday.lastfm.network

interface ConnectivityCheck {
    fun isConnected(): Boolean
}