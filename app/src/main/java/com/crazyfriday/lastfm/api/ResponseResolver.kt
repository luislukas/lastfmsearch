package com.crazyfriday.lastfm.api

import retrofit2.Response

interface ResponseResolver<T> {
    fun handleResponse(response: Response<T>, handler: ApiResponse<T>)
    fun handleErrors(e: Throwable, handler: ApiResponse<T>)
}