package com.crazyfriday.lastfm.api

interface ApiResponse<T> {
    fun onError()
    fun onNoConnection()
    fun noResults()
    fun onSuccess(t: T)
}