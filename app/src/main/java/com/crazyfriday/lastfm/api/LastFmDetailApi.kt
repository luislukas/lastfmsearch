package com.crazyfriday.lastfm.api

import com.crazyfriday.lastfm.model.Album
import com.crazyfriday.lastfm.model.LastFmDetailResponse
import com.crazyfriday.lastfm.model.LastFmResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.jetbrains.annotations.NotNull
import retrofit2.Response

class LastFmDetailApi(
        private val mApi: Api,
        private val mResponseDetailResolver: ResponseResolver<LastFmDetailResponse>) {

    private val mCompositeDisposable = CompositeDisposable()

    fun albumDetails(album: Album, @NotNull handler: ApiResponse<LastFmDetailResponse>) {
        val disposable = object : DisposableObserver<Response<LastFmDetailResponse>>() {
            override fun onComplete() {
                //NOOP
            }

            override fun onNext(response: Response<LastFmDetailResponse>) {
                mResponseDetailResolver.handleResponse(response, handler)
            }

            override fun onError(e: Throwable) {
                mResponseDetailResolver.handleErrors(e, handler)
            }
        }
        mCompositeDisposable.addAll(disposable)
        mApi.albumDetails(album.artist, album.name)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(disposable)
    }

    fun dispose() = mCompositeDisposable.clear()
}