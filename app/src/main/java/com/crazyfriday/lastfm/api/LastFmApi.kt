package com.crazyfriday.lastfm.api

import com.crazyfriday.lastfm.model.LastFmResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.jetbrains.annotations.NotNull
import retrofit2.Response

class LastFmApi(
        private val mApi: Api,
        private val mResponseResolver: ResponseResolver<LastFmResponse>) {

    private val mCompositeDisposable = CompositeDisposable()
    private var mSeach: String? = null

    fun fetch(album: String, @NotNull handler: ApiResponse<LastFmResponse>) {
        mSeach = album
        if ("".equals(mSeach)) {
            handler.noResults()
            return
        }
        val disposable = object : DisposableObserver<Response<LastFmResponse>>() {
            override fun onComplete() {
                //NOOP
            }

            override fun onNext(response: Response<LastFmResponse>) {
                mResponseResolver.handleResponse(response, handler)
            }

            override fun onError(e: Throwable) {
                mResponseResolver.handleErrors(e, handler)
            }
        }
        mCompositeDisposable.addAll(disposable)
         mApi.search(album)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(disposable)
    }

    fun refresh(@NotNull handler: ApiResponse<LastFmResponse>) {
        fetch(mSeach ?: "", handler)
    }

    fun dispose() = mCompositeDisposable.clear()
}