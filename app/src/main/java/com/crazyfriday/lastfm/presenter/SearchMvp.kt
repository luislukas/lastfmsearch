package com.crazyfriday.lastfm.presenter

import com.crazyfriday.lastfm.model.Album
import com.crazyfriday.lastfm.model.LastFmDetailResponse
import com.crazyfriday.lastfm.model.LastFmResponse

interface SearchMvp {
    interface Presenter {
        fun onAttachView(view: SearchMvp.View)
        fun onDetachView()
        fun onDestroy()
        fun search(album: String)
        fun refresh()
        fun albumDetails(album: Album)
    }

    interface View {
        fun onSuccess(response: LastFmResponse)
        fun onError()
        fun onNoConnection()
        fun displayProgress(show: Boolean)
        fun onNoResults()
        fun onDetailSuccess(response: LastFmDetailResponse)
    }
}