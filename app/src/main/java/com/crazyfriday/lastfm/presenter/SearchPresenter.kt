package com.crazyfriday.lastfm.presenter

import com.crazyfriday.lastfm.api.ApiResponse
import com.crazyfriday.lastfm.api.LastFmApi
import com.crazyfriday.lastfm.api.LastFmDetailApi
import com.crazyfriday.lastfm.model.Album
import com.crazyfriday.lastfm.model.LastFmDetailResponse
import com.crazyfriday.lastfm.model.LastFmResponse

class SearchPresenter(private val mLastFmApi: LastFmApi,
                      private val mLastFmDetailApi: LastFmDetailApi): SearchMvp.Presenter {

    private var mView: SearchMvp.View? = null

    private val mHandle = object : ApiResponse<LastFmResponse> {
        override fun onError() {
            mView?.let {
                it.displayProgress(false)
                it.onError()
            }
        }

        override fun onNoConnection() {
            mView?.let {
                it.displayProgress(false)
                it.onNoConnection()
            }
        }

        override fun onSuccess(t: LastFmResponse) {
            mView?.let {view ->
                view.displayProgress(false)
                t.results.albummatches.let {
                    if (t.results.albummatches.album.isEmpty()) {
                        view.onNoResults()
                    } else {
                        view.onSuccess(t)
                    }
                }
            }
        }

        override fun noResults() {
            mView?.let {
                it.displayProgress(false)
                it.onNoResults()
            }
        }
    }

    private val mDetailsHandle = object : ApiResponse<LastFmDetailResponse> {
        override fun onError() {
            mView?.let {
                it.displayProgress(false)
                it.onError()
            }
        }

        override fun onNoConnection() {
            mView?.let {
                it.displayProgress(false)
                it.onNoConnection()
            }
        }

        override fun onSuccess(t: LastFmDetailResponse) {
            mView?.let {
                it.displayProgress(false)
                it.onDetailSuccess(t)
            }
        }

        override fun noResults() {
            mView?.let {
                it.displayProgress(false)
                it.onNoResults()
            }
        }
    }

    override fun onAttachView(view: SearchMvp.View) {
        mView = view
        refresh()
    }

    override fun onDetachView() {
        mView = null
    }

    override fun onDestroy() {
        mLastFmApi.dispose()
        mLastFmDetailApi.dispose()
    }

    override fun search(album: String) {
        mView?.let {
            it.displayProgress(true)
            mLastFmApi.fetch(album, mHandle)
        }
    }

    override fun refresh() {
        mView?.let {
            it.displayProgress(true)
            mLastFmApi.refresh(mHandle)
        }
    }

    override fun albumDetails(album: Album) {
        mView?.let {
            it.displayProgress(true)
            mLastFmDetailApi.albumDetails(album, mDetailsHandle)
        }
    }
}