package com.crazyfriday.lastfm.di

import com.crazyfriday.lastfm.api.LastFmApi
import com.crazyfriday.lastfm.api.LastFmDetailApi
import com.crazyfriday.lastfm.presenter.SearchMvp
import com.crazyfriday.lastfm.presenter.SearchPresenter
import dagger.Module
import dagger.Provides

@Module
class ActivityModule {

    @Provides
    @ActivityScope
    fun provideSearchPresenter(lastFmApi: LastFmApi, mmLastFmDetailApi: LastFmDetailApi): SearchMvp.Presenter {
        return SearchPresenter(lastFmApi, mmLastFmDetailApi)
    }

}