package com.crazyfriday.lastfm.di

import com.crazyfriday.lastfm.lastfmsearch.MainActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {
    fun inject(activity: MainActivity)
}