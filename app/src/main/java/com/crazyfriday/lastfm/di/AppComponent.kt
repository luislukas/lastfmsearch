package com.crazyfriday.lastfm.di

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    fun provideActivityComponent(activityModule: ActivityModule): ActivityComponent
}