package com.crazyfriday.lastfm.di

import android.content.Context
import android.support.annotation.VisibleForTesting
import com.crazyfriday.lastfm.api.Api
import com.crazyfriday.lastfm.api.LastFmApi
import com.crazyfriday.lastfm.api.LastFmDetailApi
import com.crazyfriday.lastfm.api.ResponseResolver
import com.crazyfriday.lastfm.lastfmsearch.BuildConfig
import com.crazyfriday.lastfm.model.LastFmDetailResponse
import com.crazyfriday.lastfm.model.LastFmResponse
import com.crazyfriday.lastfm.network.ConnectivityCheck
import com.crazyfriday.lastfm.network.ConnectivityCheckImpl
import com.crazyfriday.lastfm.network.NoNetworkException
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = arrayOf(ResponseResolverModule::class))
open class AppModule(val mContext: Context) {

    private val BASE_URL = BuildConfig.BASE_URL

    @Provides
    @Singleton
    fun provideContext(): Context {
        return mContext
    }

    @Provides
    @Singleton
    fun provideOKHTTP(connectivityCheck: ConnectivityCheck): OkHttpClient {
        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        val networkInterceptor = object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                if (connectivityCheck.isConnected()) {
                    return chain.proceed(chain.request())
                } else {
                    throw NoNetworkException()
                }
            }
        }

        return OkHttpClient.Builder()
                .addInterceptor(networkInterceptor)
                .addInterceptor(logInterceptor)
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okhttp: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .baseUrl(getBaseURL())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okhttp).build()
    }

    @VisibleForTesting
    open fun getBaseURL(): String {
        return BASE_URL
    }

    @Provides
    @Singleton
    fun provideFeedApi(retrofit: Retrofit): Api {
        return retrofit.create(Api::class.java)
    }

    @Provides
    @Singleton
    fun provideConnectivityCheck(): ConnectivityCheck {
        return ConnectivityCheckImpl(mContext)
    }

    @Provides
    @Singleton
    fun provideLastFmSearchApi(api: Api, responseResolver: ResponseResolver<LastFmResponse>): LastFmApi {
        return LastFmApi(api, responseResolver)
    }

    @Provides
    @Singleton
    fun provideLastFmDetailApi(api: Api, responseDetailResolver: ResponseResolver<LastFmDetailResponse>): LastFmDetailApi {
        return LastFmDetailApi(api, responseDetailResolver)
    }
}