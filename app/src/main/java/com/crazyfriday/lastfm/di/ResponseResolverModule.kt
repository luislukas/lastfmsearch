package com.crazyfriday.lastfm.di

import com.crazyfriday.lastfm.api.ResponseResolver
import com.crazyfriday.lastfm.api.GenericResponseResolver
import com.crazyfriday.lastfm.model.LastFmDetailResponse
import com.crazyfriday.lastfm.model.LastFmResponse
import dagger.Module
import dagger.Provides

@Module
class ResponseResolverModule {

    @Provides
    fun provideResponseResolver(): ResponseResolver<LastFmResponse> {
        return GenericResponseResolver()
    }

    @Provides
    fun provideDetailResponseResolver(): ResponseResolver<LastFmDetailResponse> {
        return GenericResponseResolver()
    }
}