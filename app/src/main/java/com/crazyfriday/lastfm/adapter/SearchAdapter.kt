package com.crazyfriday.lastfm.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crazyfriday.lastfm.lastfmsearch.R
import com.crazyfriday.lastfm.model.Album
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_layout.*

class SearchAdapter private constructor(val mList: MutableList<Album>, val mItemClickCallback: ItemClickCallback) : RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {

    companion object {
           fun create(mList: MutableList<Album>, mItemClickCallback: ItemClickCallback): SearchAdapter {
               return SearchAdapter(mList, mItemClickCallback)
           }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        return SearchViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bindItem(mList.get(position), mItemClickCallback)
    }

    fun updateList(list: List<Album>) {
        mList.clear()
        mList.addAll(list)
        notifyDataSetChanged()
    }

    class SearchViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bindItem(item: Album, itemClickCallback: ItemClickCallback) {
            containerView.setOnClickListener { itemClickCallback.onItemClick(item) }
            mImage.setImageDrawable(ContextCompat.getDrawable(containerView.context, android.R.drawable.progress_indeterminate_horizontal))
            Log.d("SearchAdapter", "Loading image "+item.image[0].text)
            item.image[0].let {
                if (item.image[0].text.isNotEmpty()) {
                    Picasso.with(containerView.context).load(item.image[0].text).into(mImage)
                    mImage.contentDescription = containerView.context.getString(R.string.search_list_album_image_content, item.artist, item.name)
                }
            }
            mDescription.text = containerView.context.getString(R.string.search_list_album_description, item.artist, item.name)
        }
    }

    interface ItemClickCallback {
        fun onItemClick(item: Album)
    }
}