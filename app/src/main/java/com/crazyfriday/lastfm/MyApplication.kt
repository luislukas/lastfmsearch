package com.crazyfriday.lastfm.lastfmsearch

import android.app.Application
import com.crazyfriday.lastfm.di.Injector

class MyApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        Injector.init(this)
    }
}