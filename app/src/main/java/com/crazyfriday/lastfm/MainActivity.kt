package com.crazyfriday.lastfm.lastfmsearch

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.View
import android.widget.ScrollView
import com.crazyfriday.lastfm.adapter.SearchAdapter
import com.crazyfriday.lastfm.di.Injector
import com.crazyfriday.lastfm.model.Album
import com.crazyfriday.lastfm.model.LastFmDetailResponse
import com.crazyfriday.lastfm.model.LastFmResponse
import com.crazyfriday.lastfm.model.Tracks
import com.crazyfriday.lastfm.presenter.SearchMvp
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), SearchMvp.View {
    @Inject
    lateinit var mPresenter: SearchMvp.Presenter
    lateinit var mSearchView: SearchView
    lateinit var mBottomSheetBehavior: BottomSheetBehavior<ScrollView>

    private var mAdapter: SearchAdapter? = null

    private val mOnItemClickCallback = object: SearchAdapter.ItemClickCallback {
        override fun onItemClick(item: Album) {
            mPresenter.albumDetails(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Injector.injectActivity(this)
        mAdapter = SearchAdapter.create(mutableListOf(), mOnItemClickCallback)
        mRecyclerView.adapter = mAdapter
        mRecyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        mSwipeToRefesh.setOnRefreshListener {
            mPresenter.refresh()
        }

        mBottomSheetBehavior = BottomSheetBehavior.from(mBottomSheet)
        mBottomSheetBehavior.peekHeight = 0
    }

    override fun onResume() {
        super.onResume()
        mPresenter.onAttachView(this)
    }

    override fun onPause() {
        mPresenter.onDetachView()
        super.onPause()
    }

    override fun onDestroy() {
        mPresenter.onDestroy()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        mSearchView = menu.findItem(R.id.search).actionView as SearchView
        mSearchView.setSearchableInfo(
                searchManager.getSearchableInfo(componentName))
        mSearchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    mPresenter.search(query)
                }
                return true
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                //NOOP
                return false
            }
        })
        return true
    }

    override fun onSuccess(response: LastFmResponse) {
        mAdapter?.updateList(response.results.albummatches.album.asList())
        mErrorContainer.visibility = View.GONE
        mRecyclerView.visibility = View.VISIBLE
    }

    override fun onError() {
        mErrorTextMessage.text = getString(R.string.api_on_error_txt)
        mErrorContainer.visibility = View.VISIBLE
        mRecyclerView.visibility = View.GONE
    }

    override fun onNoConnection() {
        mErrorTextMessage.text = getString(R.string.api_on_no_connection_txt)
        mErrorContainer.visibility = View.VISIBLE
        mRecyclerView.visibility = View.GONE
    }

    override fun displayProgress(show: Boolean) {
        mProgress.visibility = if(show) View.VISIBLE else View.GONE
        if (mSwipeToRefesh.isRefreshing && !show) {
            mSwipeToRefesh.isRefreshing = false
        }
    }

    override fun onNoResults() {
        mErrorTextMessage.text = getString(R.string.api_on_no_results_txt)
        mErrorContainer.visibility = View.VISIBLE
        mRecyclerView.visibility = View.GONE
    }

    override fun onDetailSuccess(response: LastFmDetailResponse) {
        mImageDetail.setImageDrawable(ContextCompat.getDrawable(this, android.R.drawable.progress_indeterminate_horizontal))
        response.album.image[1].let {
            if (response.album.image[1].text.isNotEmpty()) {
                Picasso.with(this).load(response.album.image[1].text).into(mImageDetail)
            }
        }
        mDetailDescription.text = getString(R.string.search_list_album_description, response.album.artist, response.album.name)
        mTracks.text = getTracks(response.album.tracks)
        if (mBottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            mBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        } else {
            //NOOP
        }
    }

    private fun getTracks(tracks: Tracks): String {
        val stringbuilder = StringBuilder()
        for (i in tracks.track.indices) {
            stringbuilder.append(getString(R.string.detail_album_tracks, tracks.track[i].name))
        }
        return stringbuilder.toString()
    }
}
