package com.crazyfriday.lastfm.model

class Track(var duration: String, var name: String, var url: String)