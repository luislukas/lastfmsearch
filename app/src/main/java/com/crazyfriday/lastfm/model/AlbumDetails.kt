package com.crazyfriday.lastfm.model

data class AlbumDetails(val listeners: String,
                        val mbid: String,
                        val name: String,
                        val image: Array<Image>,
                        val playcount: String,
                        val tracks: Tracks,
                        val artist: String, val url: String)