package com.crazyfriday.lastfm.model

data class Album(val mbid: String,
                 val name: String,
                 val streamable: String,
                 val image: Array<Image>,
                 val artist: String,
                 val url: String)