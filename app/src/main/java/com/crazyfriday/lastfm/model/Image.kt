package com.crazyfriday.lastfm.model

import com.google.gson.annotations.SerializedName

class Image(@SerializedName("#text") var text: String, var size: String)