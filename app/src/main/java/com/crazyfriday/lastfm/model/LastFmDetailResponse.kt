package com.crazyfriday.lastfm.model

import com.google.gson.annotations.SerializedName

class LastFmDetailResponse(@SerializedName("album") var album: AlbumDetails)