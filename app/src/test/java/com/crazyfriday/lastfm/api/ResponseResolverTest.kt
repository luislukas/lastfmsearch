package com.crazyfriday.lastfm.api

import com.crazyfriday.lastfm.anyType
import com.crazyfriday.lastfm.model.LastFmResponse
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.*
import retrofit2.Response
import java.io.IOException


class ResponseResolverTest {
    private lateinit var SUT: ResponseResolver<LastFmResponse>

    private val mHandler : ApiResponse<LastFmResponse> = Mockito.mock(ApiResponse::class.java) as ApiResponse<LastFmResponse>
    private val mResponse = mock(Response::class.java) as Response<LastFmResponse>

    @Before
    fun setup() {
        SUT = GenericResponseResolver()
    }

    @Test
    fun onResponseSuccess() {
        val body = Mockito.mock(LastFmResponse::class.java)

        Mockito.`when`(mResponse.isSuccessful).thenReturn(true)
        Mockito.`when`(mResponse.body()).thenReturn(body)

        SUT.handleResponse(mResponse, mHandler)

        verify(mHandler, times(1)).onSuccess(body)
        verify(mHandler, never()).onError()
        verify(mHandler, never()).onNoConnection()
    }

    @Test
    fun onError() {
        val body = Mockito.mock(LastFmResponse::class.java)

        Mockito.`when`(mResponse.isSuccessful).thenReturn(false)

        SUT.handleResponse(mResponse, mHandler)

        verify(mHandler, times(1)).onError()
        verify(mHandler, never()).onSuccess(body)
        verify(mHandler, never()).onNoConnection()
    }

    @Test
    fun onNoConnection() {
        val error = mock(IOException::class.java)

        SUT.handleErrors(error, mHandler)

        verify(mHandler, times(1)).onNoConnection()
        verify(mHandler, never()).onSuccess(anyType())
        verify(mHandler, never()).onError()
    }

    @Test
    fun onOtherException() {
        val error = mock(Exception::class.java)

        SUT.handleErrors(error, mHandler)

        verify(mHandler, times(1)).onError()
        verify(mHandler, never()).onSuccess(anyType())
        verify(mHandler, never()).onNoConnection()
    }
}