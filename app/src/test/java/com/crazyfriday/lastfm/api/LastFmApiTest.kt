package com.crazyfriday.lastfm.api

import com.crazyfriday.lastfm.anyType
import com.crazyfriday.lastfm.model.LastFmResponse
import io.reactivex.Observable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import retrofit2.Response
import java.net.SocketTimeoutException

@Suppress("UNCHECKED_CAST")
class LastFmApiTest {
    private lateinit var SUT: LastFmApi
    private val SEARCH_VALUE = "Black"
    private val mApi = Mockito.mock(Api::class.java)
    private val mResponseResolver : ResponseResolver<LastFmResponse> = Mockito.mock(ResponseResolver::class.java) as ResponseResolver<LastFmResponse>

    @Rule
    @JvmField
    val mOverrideSchedulersRule = RxSchedulersOverrideRule()

    @Before
    fun setup() {
        SUT = LastFmApi(mApi, mResponseResolver)
    }

    @Test
    fun onSuccessWithValidSearch() {
        val responseHandler : ApiResponse<LastFmResponse> = Mockito.mock(ApiResponse::class.java) as ApiResponse<LastFmResponse>
        val response = Mockito.mock(LastFmResponse::class.java)
        val mockObservableData : Observable<Response<LastFmResponse>> = Observable.just(Response.success(response))

        Mockito.`when`(mApi.search(SEARCH_VALUE)).thenReturn(mockObservableData)

        SUT.fetch(SEARCH_VALUE, responseHandler)

        Mockito.verify(mResponseResolver, times(1)).handleResponse(anyType(), anyType())
    }

    @Test
    fun onNoCachedResponse() {
        val responseHandler : ApiResponse<LastFmResponse> = Mockito.mock(ApiResponse::class.java) as ApiResponse<LastFmResponse>

        SUT.refresh(responseHandler)

        Mockito.verify(mResponseResolver, times(0)).handleResponse(anyType(), anyType())
        Mockito.verify(responseHandler, times(1)).noResults()
    }

    @Test
    fun onError() {
        val responseHandler : ApiResponse<LastFmResponse> = Mockito.mock(ApiResponse::class.java) as ApiResponse<LastFmResponse>
        val exception : SocketTimeoutException = Mockito.mock(SocketTimeoutException::class.java)

        Mockito.`when`(mApi.search(SEARCH_VALUE)).thenReturn(Observable.error(exception))

        SUT.fetch(SEARCH_VALUE, responseHandler)

        Mockito.verify(mResponseResolver, times(1)).handleErrors(anyType(), anyType())
        Mockito.verify(mResponseResolver, never()).handleResponse(anyType(), anyType())
    }

}