package com.crazyfriday.lastfm.api

import com.crazyfriday.lastfm.anyType
import com.crazyfriday.lastfm.model.Album
import com.crazyfriday.lastfm.model.LastFmDetailResponse
import io.reactivex.Observable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock
import retrofit2.Response
import java.net.SocketTimeoutException

class LastFmDetailApiTest {
    private lateinit var SUT: LastFmDetailApi
    private val mApi = Mockito.mock(Api::class.java)
    private val mResponseResolver : ResponseResolver<LastFmDetailResponse> = Mockito.mock(ResponseResolver::class.java) as ResponseResolver<LastFmDetailResponse>
    private val mArtist = "ARTIST"
    private val mAlbum = "ALBUM"

    @Rule
    @JvmField
    val mOverrideSchedulersRule = RxSchedulersOverrideRule()

    @Before
    fun setup() {
        SUT = LastFmDetailApi(mApi, mResponseResolver)
    }

    @Test
    fun onAlbumDetailsSuccess() {
        val responseHandler : ApiResponse<LastFmDetailResponse> = Mockito.mock(ApiResponse::class.java) as ApiResponse<LastFmDetailResponse>
        val response = Mockito.mock(LastFmDetailResponse::class.java)
        val mockObservableData : Observable<Response<LastFmDetailResponse>> = Observable.just(Response.success(response))
        val mockAlbum = mock(Album::class.java)

        Mockito.`when`(mockAlbum.artist).thenReturn(mArtist)
        Mockito.`when`(mockAlbum.name).thenReturn(mAlbum)
        Mockito.`when`(mApi.albumDetails(mArtist, mAlbum)).thenReturn(mockObservableData)

        SUT.albumDetails(mockAlbum, responseHandler)

        Mockito.verify(mResponseResolver, Mockito.times(1)).handleResponse(anyType(), anyType())
    }

    @Test
    fun onError() {
        val responseHandler : ApiResponse<LastFmDetailResponse> = Mockito.mock(ApiResponse::class.java) as ApiResponse<LastFmDetailResponse>
        val exception : SocketTimeoutException = Mockito.mock(SocketTimeoutException::class.java)
        val mockAlbum = mock(Album::class.java)

        Mockito.`when`(mockAlbum.artist).thenReturn(mArtist)
        Mockito.`when`(mockAlbum.name).thenReturn(mAlbum)
        Mockito.`when`(mApi.albumDetails(mArtist, mAlbum)).thenReturn(Observable.error(exception))

        SUT.albumDetails(mockAlbum, responseHandler)

        Mockito.verify(mResponseResolver, Mockito.times(1)).handleErrors(anyType(), anyType())
        Mockito.verify(mResponseResolver, Mockito.never()).handleResponse(anyType(), anyType())
    }

}