package com.crazyfriday.lastfm.presenter

import com.crazyfriday.lastfm.api.ApiResponse
import com.crazyfriday.lastfm.api.LastFmApi
import com.crazyfriday.lastfm.api.LastFmDetailApi
import com.crazyfriday.lastfm.capture
import com.crazyfriday.lastfm.model.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers
import org.mockito.Captor
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class SearchPresenterTest {
    private lateinit var SUT: SearchPresenter
    private val mAlbum = "ALBUM"
    private val mLastFmApi = mock(LastFmApi::class.java)
    private val mLastFmDetailApi = mock(LastFmDetailApi::class.java)

    private val mView = mock(SearchMvp.View::class.java)
    private val mResponse = mock(LastFmResponse::class.java)
    private val mDetailResponse = mock(LastFmDetailResponse::class.java)

    @Captor
    private lateinit var mCaptor: ArgumentCaptor<ApiResponse<LastFmResponse>>
    @Captor
    private lateinit var mDetailCaptor: ArgumentCaptor<ApiResponse<LastFmDetailResponse>>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        SUT = SearchPresenter(mLastFmApi, mLastFmDetailApi)
    }


    @Test
    fun onAttachView() {
        SUT.onAttachView(mView)

        verify(mView, times(1)).displayProgress(true)
        verify(mLastFmApi, times(1)).refresh(capture(mCaptor))

        mCaptor.value.noResults()

        verify(mView, times(1)).displayProgress(false)
        verify(mView, times(1)).onNoResults()

        verify(mView, never()).onNoConnection()
        verify(mView, never()).onError()
    }

    @Test
    fun onDestroy() {
        SUT.onDestroy()
        verify(mLastFmApi, times(1) ).dispose()
        verify(mLastFmDetailApi, times(1) ).dispose()
    }

    @Test
    fun onSuccessSearch() {
        val mockResults = mock(Results::class.java)
        val mockAlbummatches = mock(Albummatches::class.java)
        val mockAlbums = arrayOf(mock(Album::class.java))

        SUT.onAttachView(mView)

        verify(mLastFmApi, times(1)).refresh(capture(mCaptor))

        mCaptor.value.noResults()

        verify(mView, times(1)).onNoResults()

        SUT.search(mAlbum)

        `when`(mResponse.results).thenReturn(mockResults)
        `when`(mockResults.albummatches).thenReturn(mockAlbummatches)
        `when`(mockAlbummatches.album).thenReturn(mockAlbums)

        verify(mLastFmApi, times(1)).fetch(ArgumentMatchers.anyString(), capture(mCaptor))

        mCaptor.value.onSuccess(mResponse)

        verify(mView, times(2)).displayProgress(true)
        verify(mView, times(2)).displayProgress(false)
        verify(mView, times(1)).onSuccess(mResponse)
        verify(mView, never()).onNoConnection()
        verify(mView, never()).onError()
    }

    @Test
    fun onSearchError() {
        SUT.onAttachView(mView)

        verify(mLastFmApi, times(1)).refresh(capture(mCaptor))

        mCaptor.value.noResults()

        verify(mView, times(1)).onNoResults()

        SUT.search(mAlbum)

        verify(mLastFmApi, times(1)).fetch(ArgumentMatchers.anyString(), capture(mCaptor))

        mCaptor.value.onError()

        verify(mView, times(2)).displayProgress(true)
        verify(mView, times(2)).displayProgress(false)
        verify(mView, times(1)).onError()
        verify(mView, never()).onNoConnection()
        verify(mView, never()).onSuccess(mResponse)
    }

    @Test
    fun onSearchNoConnection() {
        SUT.onAttachView(mView)

        verify(mLastFmApi, times(1)).refresh(capture(mCaptor))

        mCaptor.value.noResults()

        verify(mView, times(1)).onNoResults()

        SUT.search(mAlbum)

        verify(mLastFmApi, times(1)).fetch(ArgumentMatchers.anyString(), capture(mCaptor))

        mCaptor.value.onNoConnection()

        verify(mView, times(2)).displayProgress(true)
        verify(mView, times(2)).displayProgress(false)
        verify(mView, times(1)).onNoConnection()
        verify(mView, never()).onError()
        verify(mView, never()).onSuccess(mResponse)
    }

    @Test
    fun onDetailsSuccess() {
        val mockResults = mock(Results::class.java)
        val mockAlbummatches = mock(Albummatches::class.java)
        val mockAlbum = mock(Album::class.java)
        val mockAlbums = arrayOf(mock(Album::class.java))

        SUT.onAttachView(mView)

        verify(mLastFmApi, times(1)).refresh(capture(mCaptor))

        mCaptor.value.noResults()

        verify(mView, times(1)).onNoResults()

        SUT.search(mAlbum)

        `when`(mResponse.results).thenReturn(mockResults)
        `when`(mockResults.albummatches).thenReturn(mockAlbummatches)
        `when`(mockAlbummatches.album).thenReturn(mockAlbums)

        verify(mLastFmApi, times(1)).fetch(ArgumentMatchers.anyString(), capture(mCaptor))

        mCaptor.value.onSuccess(mResponse)

        SUT.albumDetails(mockAlbum)

        verify(mLastFmDetailApi, times(1)).albumDetails(com.crazyfriday.lastfm.any(), capture(mDetailCaptor))
        mDetailCaptor.value.onSuccess(mDetailResponse)

        verify(mView, times(3)).displayProgress(true)
        verify(mView, times(3)).displayProgress(false)
        verify(mView, times(1)).onSuccess(mResponse)
        verify(mView, times(1)).onDetailSuccess(mDetailResponse)
        verify(mView, never()).onNoConnection()
        verify(mView, never()).onError()
    }

    @Test
    fun onDetailsError() {
        val mockResults = mock(Results::class.java)
        val mockAlbummatches = mock(Albummatches::class.java)
        val mockAlbum = mock(Album::class.java)
        val mockAlbums = arrayOf(mock(Album::class.java))

        SUT.onAttachView(mView)

        verify(mLastFmApi, times(1)).refresh(capture(mCaptor))

        mCaptor.value.noResults()

        verify(mView, times(1)).onNoResults()

        SUT.search(mAlbum)

        `when`(mResponse.results).thenReturn(mockResults)
        `when`(mockResults.albummatches).thenReturn(mockAlbummatches)
        `when`(mockAlbummatches.album).thenReturn(mockAlbums)

        verify(mLastFmApi, times(1)).fetch(ArgumentMatchers.anyString(), capture(mCaptor))

        mCaptor.value.onSuccess(mResponse)

        SUT.albumDetails(mockAlbum)

        verify(mLastFmDetailApi, times(1)).albumDetails(com.crazyfriday.lastfm.any(), capture(mDetailCaptor))
        mDetailCaptor.value.onError()

        verify(mView, times(3)).displayProgress(true)
        verify(mView, times(3)).displayProgress(false)
        verify(mView, times(1)).onSuccess(mResponse)
        verify(mView, times(1)).onError()
        verify(mView, never()).onDetailSuccess(mDetailResponse)
        verify(mView, never()).onNoConnection()
    }

    @Test
    fun onDetailsNoConnection() {
        val mockResults = mock(Results::class.java)
        val mockAlbummatches = mock(Albummatches::class.java)
        val mockAlbum = mock(Album::class.java)
        val mockAlbums = arrayOf(mock(Album::class.java))

        SUT.onAttachView(mView)

        verify(mLastFmApi, times(1)).refresh(capture(mCaptor))

        mCaptor.value.noResults()

        verify(mView, times(1)).onNoResults()

        SUT.search(mAlbum)

        `when`(mResponse.results).thenReturn(mockResults)
        `when`(mockResults.albummatches).thenReturn(mockAlbummatches)
        `when`(mockAlbummatches.album).thenReturn(mockAlbums)

        verify(mLastFmApi, times(1)).fetch(ArgumentMatchers.anyString(), capture(mCaptor))

        mCaptor.value.onSuccess(mResponse)

        SUT.albumDetails(mockAlbum)

        verify(mLastFmDetailApi, times(1)).albumDetails(com.crazyfriday.lastfm.any(), capture(mDetailCaptor))
        mDetailCaptor.value.onNoConnection()

        verify(mView, times(3)).displayProgress(true)
        verify(mView, times(3)).displayProgress(false)
        verify(mView, times(1)).onSuccess(mResponse)
        verify(mView, times(1)).onNoConnection()
        verify(mView, never()).onDetailSuccess(mDetailResponse)
        verify(mView, never()).onError()
    }

}