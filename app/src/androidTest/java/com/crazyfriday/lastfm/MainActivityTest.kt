package com.crazyfriday.lastfm

import android.os.SystemClock
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import com.crazyfriday.lastfm.lastfmsearch.MainActivity
import com.crazyfriday.lastfm.lastfmsearch.R

import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    private val ALBUM_SEARCH_TEXT = "Believe\n"
    private val VALID_ALBUM = "Make Believe"
    private val MODEL = "valid_album_response.json"

    @Rule
    @JvmField
    val mTestRule = MockTestRule(MainActivity::class.java, true, false)

    @After
    fun tearDown() {
        mTestRule.shutdown()
    }

    @Test
    fun onSucessfulSearchDisplaysResultsCorrectly() {
        mTestRule.launchActivity(null)
        onView(withText(mTestRule.activity.getString(R.string.api_on_no_results_txt))).check(matches(isDisplayed()))
        onView(withId(R.id.search)).perform(click())
        mTestRule.enqueue(mTestRule.loadValidResponse(MODEL))
        onView(withId(android.support.design.R.id.search_src_text)).perform(typeText(ALBUM_SEARCH_TEXT))
        SystemClock.sleep(1000)
        onView(withId(R.id.mRecyclerView)).check(matches(hasDescendant(withText(containsString(VALID_ALBUM)))))
    }


    @Test
    fun onTimeoutDisplaysNoConnection() {
        mTestRule.launchActivity(null)
        onView(withText(mTestRule.activity.getString(R.string.api_on_no_results_txt))).check(matches(isDisplayed()))
        onView(withId(R.id.search)).perform(click())
        mTestRule.enqueueTimeout()
        onView(withId(android.support.design.R.id.search_src_text)).perform(typeText(ALBUM_SEARCH_TEXT))
        onView(withText(containsString(mTestRule.activity.getString(R.string.api_on_no_connection_txt)))).check(matches(isDisplayed()))
        onView(withId(R.id.mRecyclerView)).check(matches(not(isDisplayed())))
    }


    @Test
    fun onErrorDisplaysErrorMessage() {
        mTestRule.launchActivity(null)
        onView(withText(mTestRule.activity.getString(R.string.api_on_no_results_txt))).check(matches(isDisplayed()))
        onView(withId(R.id.search)).perform(click())
        mTestRule.enqueueError()
        onView(withId(android.support.design.R.id.search_src_text)).perform(typeText(ALBUM_SEARCH_TEXT))
        onView(withText(containsString(mTestRule.activity.getString(R.string.api_on_error_txt)))).check(matches(isDisplayed()))
        onView(withId(R.id.mRecyclerView)).check(matches(not(isDisplayed())))
    }
}